/*
* FeedEk jQuery RSS/ATOM Feed Plugin v3.0 with YQL API
* http://jquery-plugins.net/FeedEk/FeedEk.html  https://github.com/enginkizil/FeedEk
* Author : Engin KIZIL http://www.enginkizil.com   
*/

(function ($) {
    $.fn.FeedEk = function (opt) {
        var def = $.extend({
            MaxCount: 5,
            ShowDesc: true,
            ShowPubDate: true,
            DescCharacterLimit: 0,
            TitleLinkTarget: "_blank",
            DateFormat: "",
            DateFormatLang:"en"
        }, opt);
        
        var id = $(this).attr("id"), i, s = "", dt;
        $("#" + id).empty();
        if (def.FeedUrl == undefined) return;       
    

        var YQLstr = 'SELECT channel.item FROM feednormalizer WHERE output="rss_2.0" AND url ="' + def.FeedUrl + '" LIMIT ' + def.MaxCount;

        $.ajax({
            url: "https://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent(YQLstr) + "&format=json&diagnostics=false&callback=?",
            dataType: "json",
            success: function (data) {
                $("#" + id).empty();
                if (!(data.query.results.rss instanceof Array)) {
                    data.query.results.rss = [data.query.results.rss];
                }
                $.each(data.query.results.rss, function (e, itm) {
                    fecha ="";
                    description ="";

                    titulo = '<a href="' + itm.channel.item.link + '" target="' + def.TitleLinkTarget + '" >' + itm.channel.item.title + '</a>';
                    
                    if (def.ShowPubDate){
                        dt = new Date(itm.channel.item.pubDate);
                        if ($.trim(def.DateFormat).length > 0) {
                            try {
                                moment.lang(def.DateFormatLang);
                                fecha = moment(dt).format(def.DateFormat);
                            }
                            catch (e){fecha = dt.toLocaleDateString();}                            
                        }
                        else {
                            fecha = dt.toLocaleDateString();
                        }
                    }
                    if (def.ShowDesc) {
                         if (def.DescCharacterLimit > 0 && itm.channel.item.description.length > def.DescCharacterLimit) {
                           description = itm.channel.item.description.substring(0, def.DescCharacterLimit) + '...';
                        }
                        else {
                           description = itm.channel.item.description;
                         }
                    }

                            s += ' <blockquote class="card-blockquote"><div class="card">\
                                <div class="card-block">\
                                <h4 class="card-title">'+titulo+'</h4>\
                                <p class="card-text" >'+description+'</p>\
                                <p class="card-text"><small class="text-muted">Fecha: '+fecha+'</small></p>\
                                </div>\
                                </div>'
                });
                $("#" + id).append(s);
            }
        });
    };
})(jQuery);


