# Full path and name to your csv file
csv_filepathname="C:/ProyectoAdiuvo/adiuvomejorado/corregimientos.csv"
# Full path to your django project directory
home="C:/ProyectoAdiuvo/adiuvomejorado/ProyectoAdiuvo/"


import sys,os
sys.path.append(home)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django
django.setup()


from localizacion.models import vereda, municipio


import csv
dataReader = csv.reader(open(csv_filepathname), delimiter=';', quotechar='"')
def utf8_encode(val):
    try:
        tmp = val.decode('utf8')
    except:
        try:
            tmp = val.decode('latin1')
        except:
            tmp= val.decode('ascii', 'ignore')
            tmp = tmp.encode('utf8')    
    return tmp
for row in dataReader:
    
    depto = vereda()
    depto.es_municipio_pk = municipio.objects.get(A_codigoDane=row[0])
    depto.A_codigoDane = row[1]
    depto.A_nombre = row[2]
    
    print "%s - %s - %s" % (depto.es_municipio_pk,depto.A_codigoDane,depto.A_nombre)
    depto.save()
