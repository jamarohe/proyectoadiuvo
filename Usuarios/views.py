# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib.auth import authenticate,login
from django.contrib.auth.models import User
from .models import *
from django.shortcuts import redirect
from django.contrib.auth.models import Group
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse

def showSignUp(request):
	return render(request,"registro.html")

@csrf_exempt
def signUp(request):
	if request.method == "POST":
		user_name = request.POST['username']
		apellido = request.POST['apellido']
		correo = request.POST['correo']
		contrasena = request.POST['contrasena']
		nombre = request.POST['nombre']
		profecion = request.POST['profecion']
		entidad = request.POST['entidad']
		fecha_na = request.POST['fecha_na']
		genero = request.POST['genero']
		universidad = request.POST['universidad']
		titulo = request.POST['titulo']
		profe = True
		staff = True

		try:
			obj_user = ob_usuarios.objects.create_user(username=user_name,first_name=nombre,last_name=apellido,password=contrasena,
				email=correo,us_eres_profe=profe,us_max_titulo=titulo,us_universidad=universidad,us_genero=genero,us_fecha_na=fecha_na,
				us_profecion=profecion,us_entidad=entidad, is_staff=staff)
			grupo_postulante = Group.objects.get(name="POSTULANTE")
			obj_user.save()
			obj_user.groups.add(grupo_postulante)
			return HttpResponse("registrado")
		except Exception as e:
			print (e)
			return HttpResponse("existe user")
	return HttpResponse("error")

# Create your views here.
def enviar_correo_comunicaciones(mensaje, email):
    fecha_actual = datetime.datetime.now()
    template_html = 'correo_comunicaciones.html'
    html_content = render_to_string(template_html, {'fecha': fecha_actual, 'mensaje': mensaje})
    subject = u" adiuvocolombia - ALGUIEN QUIERE COMUNICARSE CONTIGO DESDE ADIUVO"
    text_content = '' 
    from_email = '"No responder " <noreply.adiuvo@gmail.com>'
    to = email
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()




@csrf_exempt
@login_required
def guardar_notificaciones (request):
    error = False
    if request.method  == "POST":
        para = request.POST['para']
        titulo = request.POST['titulo']

        mensaje = request.POST['mensaje']
        if (para!=None and para!= ""):
            notificaciones_obj = notificaciones()
            notificaciones_obj.usuario = request.user
            notificaciones_obj.para = ob_usuarios.objects.get(pk = para)
            if (titulo!=None and titulo!=""):
                notificaciones_obj.mensaje = mensaje + "<br>" + propuesta.objects.get(pk=titulo).titulo
            else:
                notificaciones_obj.mensaje = mensaje
            
            pool = Pool(processes=1)
            pool.apply_async(
                enviar_correo_comunicaciones,
                args=[notificaciones_obj.mensaje,notificaciones_obj.usuario.email],
                callback=callback_correo
            )
            notificaciones_obj.save()
        else:
            error =True
    else:
        error =True


    json = []
    proyectoDir = {}
    if (error):
        proyectoDir['status'] = "ERROR"
    else:
        proyectoDir['status'] = "OK"

    json.append(proyectoDir)
    return JsonResponse(json, safe=False)




@csrf_exempt
@login_required
def enivar_notificaciones (request):
    json = []
    if request.method  == "GET":
        notificaciones_obj = notificaciones.objects.filter(para=request.user, estado=False).order_by('-D_fechaCreacion')
        for notificacion in notificaciones_obj:
            try:
                proyectoDir = {}
                proyectoDir['de'] = "(%s) %s %s" %(notificacion.usuario.username, notificacion.usuario.first_name,notificacion.usuario.last_name)
                proyectoDir['mensaje'] = notificacion.mensaje
                proyectoDir['pk'] = notificacion.pk
                proyectoDir['fecha'] = notificacion.D_fechaCreacion.strftime('%Y-%m-%d') 
                json.append(proyectoDir)
            except Exception as e:
                raise e

    return JsonResponse(json, safe=False)



@csrf_exempt
def actualizar_noti (request):
    error = False
    if request.method  == "POST":
        pk = request.POST['pk']
        if (pk!=None and pk!= ""):
            notificaciones_obj = notificaciones.objects.get(pk=pk)
            notificaciones_obj.estado = True
            notificaciones_obj.save()
        else:
            error =True
    else:
        error =True

    json = []
    proyectoDir = {}
    if (error):
        proyectoDir['status'] = "ERROR"
    else:
        proyectoDir['status'] = "OK"

    json.append(proyectoDir)
    return JsonResponse(json, safe=False)

