# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *
from django.contrib.auth.admin import UserAdmin
from django.contrib import admin

class UserSignup(UserAdmin):
	list_display = ('username', 'first_name','last_name','us_profecion','us_entidad','us_fecha_na','us_genero','us_universidad',
		'us_max_titulo','us_eres_profe','is_staff','is_active')
	search_fields = ['username','us_profecion','us_genero',]
	list_filter = ('username','us_profecion','us_genero')
	fieldsets = (
		('Datos personales', {
			'fields': ('username', 'first_name', 'last_name', 'us_fecha_na','us_genero','password')
		}),
		('Datos profesionales', {
			'fields': ('us_profecion', 'us_entidad', 'us_universidad', 'us_max_titulo','us_eres_profe')
		}),
		('Datos de loggin', {
			'fields': ('is_staff', 'is_active', 'last_login', 'date_joined','groups')
		}),
	)



class notificacionesAdmin(admin.ModelAdmin):
    list_display = ('usuario','para', 'mensaje', 'D_fechaCreacion')

    def save_model(self, request, obj, form, change):
        if change==False:
            obj.usuario = request.user
        obj.save()

    def get_readonly_fields(self, request, obj=None):
    # make all fields readonly
        if request.user.is_superuser==False:
            readonly_fields = ['usuario','para','mensaje']
            if 'is_submitted' in readonly_fields:
                readonly_fields.remove('is_submitted')
            return readonly_fields
      
        return []

    def get_queryset(self, request):
        qs = super(notificacionesAdmin, self).get_queryset(request)
        if len(request.user.groups.all()) > 0:
            if request.user.groups.all()[0].name == "POSTULANTE" :
                try:
                    used= qs.filter(para__pk = request.user.pk)
                except:
                    used= qs.filter(pk =0)
            elif request.user.is_superuser:
                try:
                    used= qs.filter(Q(para__pk=request.user.pk) | Q(usuario__pk=request.user.pk))
                except:
                    used= qs.filter(pk =0)         
            else:
                used=qs.filter(pk =0)
            
            return used

        elif request.user.is_superuser:
            return qs


admin.site.register(ob_usuarios,UserSignup)
admin.site.register(notificaciones,notificacionesAdmin)

# Register your models here.
