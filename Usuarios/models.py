# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models
from Proyectos.models import *

class ob_usuarios(AbstractUser):
	us_fecha_creacion = models.DateTimeField(auto_now_add=True,verbose_name="Fecha de creacion")
	us_profecion = models.CharField(verbose_name='Profesión',blank=True,null=True,max_length=200)
	us_entidad = models.CharField(verbose_name="Entidad", blank=True,null=True,max_length=200)
	us_fecha_na = models.DateField(verbose_name="Fecha de cumpleaños",blank=True,null=True,max_length=300)
	GENERO = (
		('M',"Masculino"),
		('F',"Femenino"),
		('O',"Otro"),
		)
	us_genero = models.CharField(verbose_name="Género",max_length=20,blank=True,null=True,choices=GENERO)
	us_universidad = models.CharField(verbose_name="Universidad",max_length=200,blank=True,null=True)
	us_max_titulo = models.CharField(verbose_name="Máximo titulo alcanzado",max_length=200,blank=True,null=True)
	us_eres_profe = models.BooleanField(verbose_name="Eres profesor",default=False)

	class Meta:
		verbose_name="Usuario"
		verbose_name_plural="Usuarios"


class notificaciones(models.Model):
	usuario=models.ForeignKey('ob_usuarios',blank=True,null=True,verbose_name="De")
	para=models.ForeignKey('ob_usuarios',blank=True,null=True,verbose_name="Para", related_name="para")
	mensaje=models.TextField(max_length=255,blank=True,null=True,verbose_name="Mensaje")
	estado=models.BooleanField(verbose_name="visto", default=False)
	D_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación",auto_now_add=True, blank = True, null=True)


	def __unicode__(self):
			return u"De: %s Para: %s" %(self.usuario, self.para)
			
	def __str__(self):
			return u"De: %s Para: %s" %(self.usuario, self.para)
	class Meta:
		verbose_name=u'Notificación'
		verbose_name_plural=u'Notificaciones'





