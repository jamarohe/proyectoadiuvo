# Full path and name to your csv file
csv_filepathname="C:/ProyectoAdiuvo/adiuvomejorado/departamentos.csv"
# Full path to your django project directory
home="C:/ProyectoAdiuvo/adiuvomejorado/ProyectoAdiuvo/"


import sys,os
sys.path.append(home)
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django
django.setup()

from localizacion.models import departamento


import csv
dataReader = csv.reader(open(csv_filepathname), delimiter=';', quotechar='"')
def utf8_encode(val):
    try:
        tmp = val.decode('utf8')
    except:
        try:
            tmp = val.decode('latin1')
        except:
            tmp= val.decode('ascii', 'ignore')
            tmp = tmp.encode('utf8')    
    return tmp

for row in dataReader:
    
    
    depto = departamento()
    depto.A_codigoDane = row[0]
    depto.A_nombre = utf8_encode(row[1])
    
    print "%s " % (depto.A_codigoDane)
    depto.save()
