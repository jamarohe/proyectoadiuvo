from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^return_vereda/', views.return_vereda, name="api-get-veredas"),
	url(r'^return_departamento/', views.return_departamento, name="api-get-municipios"),

]