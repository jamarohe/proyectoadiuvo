# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from localizacion.models import *

# Register your models here.

admin.site.register(departamento)
admin.site.register(municipio)
admin.site.register(vereda)

