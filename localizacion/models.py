# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class departamento(models.Model):
	A_codigoDane = models.IntegerField(verbose_name="Código ")
	A_nombre=models.CharField(max_length=100,verbose_name="Nombre ")
	DT_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación ",auto_now_add=True, blank = True, null=True)

	def __unicode__(self):
			return u"%s" %(self.A_nombre)

	def __str__(self):
			return u"%s" %(self.A_nombre)
	class Meta:
		verbose_name=u'Departamento'
		verbose_name_plural=u'Departamentos'


class municipio(models.Model):
	A_codigoDane = models.IntegerField(verbose_name="Código ")
	es_departamento_pk = models.ForeignKey(departamento,verbose_name="Departamento ")
	A_nombre=models.CharField(max_length=100,verbose_name="Nombre ")
	DT_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación ",auto_now_add=True, blank = True, null=True)

	def __unicode__(self):
			return u"%s" %(self.A_nombre)

	def __str__(self):
			return u"%s" %(self.A_nombre)
	class Meta:
		verbose_name=u'Municipio'
		verbose_name_plural=u'Municipios'


class vereda(models.Model):
	A_codigoDane = models.IntegerField(verbose_name="Código ")
	A_nombre=models.CharField(max_length=100,verbose_name="Nombre ")
	DT_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación ",auto_now_add=True, blank = True, null=True)
	es_municipio_pk = models.ForeignKey(municipio,verbose_name="Municipio ")

	def __unicode__(self):
			return u"%s" %(self.A_nombre)
	class Meta:
		verbose_name=u'Vereda'
		verbose_name_plural=u'Veredas'
		

