# -*- coding: utf-8 -*-
from suit.apps import DjangoSuitConfig

from suit.menu import ParentItem, ChildItem


class SuitConfig(DjangoSuitConfig):
    menu = (
        ParentItem("Convocatorias", children=[
            ChildItem(model='Proyectos.convocatoria'),
        ]),
        ParentItem('Proyectos', children=[
            ChildItem(model='Proyectos.propuesta'),
            ChildItem(model='Proyectos.propuesta_tematica'),
            #ChildItem(model='proyectos.actividades'),
            #ChildItem(model='proyectos.gastos'),
            #ChildItem(model='proyectos.sos_oportunidad_negocio'),
            #ChildItem(model='proyectos.r_esperado'),
            #ChildItem(model='proyectos.r_impacto'),
            #ChildItem(model='proyectos.ultimo_proyecto'),  

        ], icon='fa fa-users'),


        ParentItem('Red', children=[
            ChildItem(model='Usuarios.ob_usuarios'),
            ChildItem('Roles', 'auth.group'),
            ChildItem('Comunicaciones', 'Usuarios.notificaciones'),
            ChildItem('Respuesta encuestas', 'Proyectos.encuestas'),
            ChildItem('Buzon de sugerencias', 'Proyectos.sugerencias'),
            

        ], align_right=True, icon='fa fa-cog'),

        ParentItem(u'Montar Documentos', children=[
            ChildItem(model='Proyectos.documentos_importante'),
            

        ], icon='fa fa-cog'),
        
    )

    def ready(self):
        super(SuitConfig, self).ready()

        # DO NOT COPY FOLLOWING LINE
        # It is only to prevent updating last_login in DB for demo app
        self.prevent_user_last_login()

    def prevent_user_last_login(self):
        """
        Disconnect last login signal
        """
        from django.contrib.auth import user_logged_in
        from django.contrib.auth.models import update_last_login
        user_logged_in.disconnect(update_last_login)




