# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from .models import *
from django.views.decorators.csrf import csrf_exempt
from django.views import generic
from .models import encuestas
from .models import sugerencias
from django.conf import settings
from io import BytesIO
from django.views.generic import View, ListView
from Proyectos.utils import render_to_pdf #created in step 4
from django.shortcuts import render, get_object_or_404


# @staff_member_required
# def get_queryset(self):
#     return convocatoria.objects


@staff_member_required
@login_required(login_url='/')
@csrf_exempt
def postulacion_a_convocatoria(request,pk):
    if(request.method=="GET"):
        convocatoria_nombre = get_object_or_404(convocatoria, pk=pk)
        tematicas_pk = propuesta_tematica.objects.all()
        context_dict = {'tematicas': tematicas_pk}

        return render(request, "post_a_convocatoria.html", {'convocatoria':convocatoria_nombre, 'tematicas':tematicas_pk})

@staff_member_required
@login_required(login_url='/')
@csrf_exempt
def save_propuesta(request):
    json =[]
    pk_new = 0
    if request.method == "POST":
        propuesta_pk = request.POST['propuesta_pk']
        convocatoria_ = request.POST['pk_convocatoria']
        titulo_ = request.POST['titulo_propuesta']
        resumen_ = request.POST['resumen_propuesta']
        justificacion_ = request.POST['justificacion']
        pregunta_ = request.POST['pregunta_propuesta']
        tematica_ = request.POST['tematica']
        solucion_ = request.POST['solucion_propuesta']
        deter_ = request.POST.get("determinantes")
        if (propuesta_pk=="NaN"):
            try:
                propuesta_check = propuesta.objects.filter(convocatoria__pk=convocatoria_ , usuario = request.user)[0]
                mensaje = "ya-postulado"
                pk_new = propuesta_check.pk


                status_dir = {}
                status_dir["status"] = mensaje

                status_dir["pk_propuesta"] = pk_new
                json.append(status_dir)
                
            except Exception as e:
                
                obj_post = propuesta()
                mensaje = "insert"    
                obj_post.convo_nombre = convocatoria.objects.get(pk=convocatoria_)
                obj_post.user = request.user
                obj_post.prop_titulo = titulo_
                obj_post.prop_resumen_desafio = resumen_
                obj_post.prop_pregunta_desafio = pregunta_
                determinants = deter_.replace(';', ',')
                obj_post.det_determinantes = determinants
                obj_post.tem_tematicas = propuesta_tematica.objects.get(pk = tematica_)
                obj_post.prop_justificacion = justificacion_
                obj_post.prop_solucion = solucion_
                obj_post.save()

                status_dir = {}
                status_dir["status"] = mensaje
                status_dir["pk_propuesta"] = obj_post.pk
                status_dir["determinants"] = obj_post.det_determinantes
                json.append(status_dir)
                return JsonResponse(json, safe=False)
        else:
            obj_post = propuesta.objects.get(pk = propuesta_pk)
            mensaje = "update"

            obj_post.convo_nombre = convocatoria.objects.get(pk=convocatoria_)
            obj_post.user = request.user
            obj_post.prop_titulo = titulo_
            obj_post.prop_resumen_desafio = resumen_
            obj_post.prop_pregunta_desafio = pregunta_
            obj_post.tem_tematicas = propuesta_tematica.objects.get(pk = tematica_)
            obj_post.prop_justificacion = justificacion_
            obj_post.prop_solucion = solucion_
            obj_post.save()

            status_dir = {}
            status_dir["status"] = mensaje
            status_dir["pk_propuesta"] = obj_post.pk
            json.append(status_dir)
            return JsonResponse(json, safe=False)
    return JsonResponse(json, safe=False)


@login_required(login_url='/')
@csrf_exempt
def view_deter(request):
    propuesta_check = propuesta.objects.get(pk=pk)
    deter_obj = propuesta_check.det_determinantes
    return render(request, "post_a_convocatoria.html")
    


@staff_member_required
@login_required(login_url='/')
@csrf_exempt
def guardar_actividad(request): 
    json = []
    dtr = ""
    pk_new = 0
    if request.method=="POST":
        propuesta_pk = request.POST['propuesta_pk']
        voluntario = request.POST['voluntario_']
        actividad_text = request.POST['actividad_']
        duracion = request.POST['duracion_']
        metodologia = request.POST['metodologia']
        riesgos = request.POST['riesgos_']
        presupuesto = request.POST['presupuesto_']
        tipo_voluntario = request.POST['tipo_voluntario']
        # pk_actividad_ = request.POST['pk_actividad_']
        cotizacion = request.FILES['cotizacion_']
        propuesta_check = propuesta.objects.get(pk=propuesta_pk)

        # pos_ = None
        # actividad_ = None

        pos_ = actividade.objects.filter(act_propuesta=propuesta_check).count()
        
        actividad_ = actividade()
        actividad_.act_propuesta = propuesta_check
        actividad_.act_actividades = actividad_text
        actividad_.act_metodologia = metodologia
        actividad_.act_tiempo = duracion
        actividad_.act_presupuesto = presupuesto
        actividad_.act_cotizacion = cotizacion
        actividad_.act_riesgo = riesgos
        if voluntario == "SI":
            actividad_.act_voluntario = True
        elif voluntario == "NO":
            actividad_.act_voluntario = False
        actividad_.act_tipo_voluntario = tipo_voluntario
        actividad_.save()
        actividad_obj = actividade.objects.filter(act_propuesta=propuesta_check)
        total = 0
        for actividad in actividad_obj:
            total += int(actividad.act_presupuesto)

        mensaje = "insert"
        pk_new = propuesta_check.pk            
        status_dir = {}
        status_dir["status"] = mensaje

        # propuesta_check.presupuesto = total
        # propuesta_check.save()
        # print propuesta_check.presupuesto


        # if (pk_actividad_==""):
        status_dir["es_nuevo"] = "SI"
        status_dir["pk_actividad"] = actividad_.pk
        status_dir["actividad_"] = actividad_.act_actividades
        status_dir["riesgos_"] = actividad_.act_riesgo
        status_dir["metodologia"] = actividad_.act_metodologia
        status_dir["actividad_"] = actividad_.act_actividades 
        status_dir["duracion_"] = actividad_.act_tiempo 
        status_dir["presupuesto_"] = "$ "+str('{:,}'.format(float(actividad_.act_presupuesto)))    
        status_dir["presupuesto"] ="$ "+str('{:,}'.format(total))  
        # else:
        #     status_dir["es_nuevo"] = "NO"

        #     status_dir["pk_actividad"] = actividad_.pk 
        #     status_dir["riesgos_"] = actividad_.act_riesgo
        #     status_dir["metodologia"] = actividad_.act_metodologia 
        #     status_dir["cotizacion_"] = actividad_.act_cotizacion
        #     status_dir["actividad_"] = actividad_.act_actividades 
        #     status_dir["duracion_"] = actividad_.act_tiempo 
        #     status_dir["presupuesto_"] = "$ "+str('{:,}'.format(float(actividad_.act_presupuesto)))    
        #     status_dir["presupuesto"] ="$ "+str('{:,}'.format(total))  

        status_dir["pk_propuesta"] = pk_new

        json.append(status_dir)
    print dtr
    
    return JsonResponse(json,safe=False)
    




@login_required(login_url='/')
@csrf_exempt
def guardar_resultados_esperados(request): 
    json =[]
    pk_new = 0
    if (request.method=="POST"):
        propuesta_pk = request.POST['propuesta_pk']
        resultados_esperado_ = request.POST['resultados_esperado_']
        cantidad_resultado_esperado = request.POST['cantidad_resultado_esperado']
        unidad_resultado_esperado = request.POST['unidad_resultado_esperado']
        
        propuesta_check = propuesta.objects.get(pk=propuesta_pk)
        resultado = r_esperado()
        resultado.unidad = unidad_resultado_esperado
        resultado.cantidad = cantidad_resultado_esperado
        resultado.resultados_esperados = resultados_esperado_
        resultado.usuario = request.user
        resultado.propuesta = propuesta_check
        resultado.save()

        mensaje = "insert"
        pk_new = propuesta_check.pk
        status_dir = {}
        status_dir["status"] = mensaje

        status_dir["resultados_esperado_"] = resultado.resultados_esperados
        status_dir["cantidad_resultado_esperado"] = resultado.cantidad
        status_dir["unidad_resultado_esperado"] = resultado.unidad 
        status_dir["pk_resultado_esperado"] = resultado.pk

        status_dir["pk_propuesta"] = pk_new
        json.append(status_dir) 

 
    return JsonResponse(json,safe=False) 




@login_required(login_url='/')
@csrf_exempt 
def get_datos_actividad(request): 
    json =[]
    pk_new = 0
    if (request.method=="POST"):
        pk_actividad = request.POST['pk_actividad']
        pk_propuesta = request.POST['pk_propuesta']
        mensaje =""
        actividad  = actividade.objects.get(pk=pk_actividad)
        
        if actividad!=None:
            status_dir = {}
            status_dir["pk"] = actividad.pk
            status_dir["riesgos_"] = actividad.act_riesgo
            status_dir["metodologia"] = actividad.act_metodologia 
            # status_dir["cotizacion_"] = actividad.act_cotizacion
            status_dir["actividad_"] = actividad.act_actividades 
            status_dir["duracion_"] = actividad.act_tiempo
            status_dir["presupuesto_"] = actividad.act_presupuesto
            # status_dir["des_ayuda_tecnica"] = actividad.des_ayuda_tecnica
            # status_dir["etapa_actividad"] = tipo
            

            json.append(status_dir) 

    return JsonResponse(json,safe=False) 


@login_required(login_url='/')
@staff_member_required       
def info_post(request,pk):
    informacion = get_object_or_404(propuesta, pk=pk)
    return render(request,"info_postulacion.html", {'informacion': informacion}) 


@login_required(login_url='/')
@staff_member_required       
def sugerencia(request):
    return render(request,"admin/sugerencia.html") 

@csrf_exempt
def enviar_sugerencia(request):
    if request.method == 'POST':
        user = request.user
        Sugerencia = request.POST['Sugerencia']
        try:
            obj_sugerencia = sugerencias()
            obj_sugerencia.user = user
            obj_sugerencia.Sugerencia = Sugerencia
            obj_sugerencia.save()
            return HttpResponse("enviado")
        except Exception as e:
            raise e
            return HttpResponse("error")

    if request.method == "POST":
        usuario = request.user
        pregunta1 = request.POST['pregunta1']
        Pregunta2 = request.POST['pregunta2']
        Pregunta3 = request.POST['pregunta3']
        Pregunta4 = request.POST['pregunta4']
        Pregunta5 = request.POST['pregunta5']
        Pregunta6 = request.POST['pregunta6']
        Pregunta7 = request.POST['pregunta7']
        Pregunta8 = request.POST['pregunta8']

        try:
            obj_encuesta = encuestas()
            obj_encuesta.usuario=usuario
            obj_encuesta.pregunta1=pregunta1
            obj_encuesta.Pregunta2=Pregunta2
            obj_encuesta.Pregunta3=Pregunta3
            obj_encuesta.Pregunta4=Pregunta4
            obj_encuesta.Pregunta5=Pregunta5
            obj_encuesta.Pregunta6=Pregunta6
            obj_encuesta.Pregunta7=Pregunta7
            obj_encuesta.Pregunta8=Pregunta8
            obj_encuesta.save()
            return HttpResponse("guardado")
        except Exception as e:
            print (e)
            return HttpResponse("existe encuesta")

@login_required(login_url='/')
@staff_member_required       
def encuesta(request):
    encuesta = encuestas.objects.all()
    content = {'encuesta': encuesta}
    return render(request,"admin/encuesta.html", content ) 
@csrf_exempt
def realizar_encuesta(request):

    if request.method == "POST":
        usuario = request.user
        pregunta1 = request.POST['pregunta1']
        Pregunta2 = request.POST['pregunta2']
        Pregunta3 = request.POST['pregunta3']
        Pregunta4 = request.POST['pregunta4']
        Pregunta5 = request.POST['pregunta5']
        Pregunta6 = request.POST['pregunta6']
        Pregunta7 = request.POST['pregunta7']
        Pregunta8 = request.POST['pregunta8']

        try:
            obj_encuesta = encuestas()
            obj_encuesta.usuario=usuario
            obj_encuesta.pregunta1=pregunta1
            obj_encuesta.Pregunta2=Pregunta2
            obj_encuesta.Pregunta3=Pregunta3
            obj_encuesta.Pregunta4=Pregunta4
            obj_encuesta.Pregunta5=Pregunta5
            obj_encuesta.Pregunta6=Pregunta6
            obj_encuesta.Pregunta7=Pregunta7
            obj_encuesta.Pregunta8=Pregunta8
            obj_encuesta.save()
            return HttpResponse("guardado")
        except Exception as e:
            print (e)
            return HttpResponse("existe encuesta")



class IndexView(ListView):
    template_name = "index.html"
    model = propuesta
    context_object_name = "Detalle propuesta"

@login_required(login_url='/')
def generar_pdf(request, pdf_pk):
    data = get_object_or_404(propuesta, pk=pdf_pk)
    pdf = render_to_pdf('Reporte.html', {'data': data})
    return HttpResponse(pdf, content_type='application/pdf')
 


@csrf_exempt
@login_required
@staff_member_required
def enviar_documentos (request):
    json = []
    if request.method  == "GET":
        doc_obj = documentos_importante.objects.filter(estado=True)
        for doc in doc_obj:
            try:
                propuestaDir = {}
                propuestaDir['url'] = doc.documento.url
                propuestaDir['des'] = doc.descripcion
                propuestaDir['fecha'] = doc.D_fechaCreacion.strftime('%Y-%m-%d') 
                json.append(propuestaDir)
            except Exception as e:
                pass
    return JsonResponse(json, safe=False)


@staff_member_required
def get_documeto(request):
    json = []
    if request.method  == "GET":
        documentos_importante_obj = documentos_importante.objects.get(codigo="DOCUMENTO_EVALUACION")
        propuestaDir = {}
        propuestaDir['url'] = documentos_importante_obj.documento.url
        propuestaDir['des'] = documentos_importante_obj.descripcion
        json.append(propuestaDir)
    return JsonResponse(json, safe=False)



@login_required
@csrf_exempt
@staff_member_required
def get_evaluacion(request):


    if(request.method=='POST'):
        json=[]
        pk_propuesta=request.POST["pk_propuesta"]
        try:
            evaluacion_obj = evaluacion.objects.filter(propuesta__pk=pk_propuesta)[0]
        except:
            return JsonResponse(json,safe=False)
        try:
            evaluacion_objDir = {}
            evaluacion_objDir['problematica_nota']=evaluacion_obj.problematica
            evaluacion_objDir['observacion_problematica_str']=evaluacion_obj.observacion_problematica
            evaluacion_objDir['justificacion_nota']=evaluacion_obj.justificacion
            evaluacion_objDir['observacion_justificacion_str']=evaluacion_obj.observacion_justificacion
            evaluacion_objDir['accion_nota']=evaluacion_obj.accion
            evaluacion_objDir['observacion_accion_str']=evaluacion_obj.observacion_accion
            evaluacion_objDir['metodologia_nota']=evaluacion_obj.metodologia
            evaluacion_objDir['observacion_metodologia_str']=evaluacion_obj.observacion_metodologia
            evaluacion_objDir['r_esperado_nota']=evaluacion_obj.r_esperado
            evaluacion_objDir['observacion_r_esperado_str']=evaluacion_obj.observacion_r_esperado
            evaluacion_objDir['actividades_nota']=evaluacion_obj.actividades
            evaluacion_objDir['observacion_actividades_str']=evaluacion_obj.observacion_actividades
            evaluacion_objDir['posibles_riesgos_nota']=evaluacion_obj.posibles_riesgos
            evaluacion_objDir['observacion_posibles_riesgos_str']=evaluacion_obj.observacion_posibles_riesgos
            evaluacion_objDir['nombre_propuesta']=evaluacion_obj.propuesta.prop_titulo


            evaluacion_objDir['beneficiarios_nota']=evaluacion_obj.beneficiarios
            evaluacion_objDir['observacion_beneficiarios_str']=evaluacion_obj.observacion_beneficiarios

            evaluacion_objDir['presupuesto_nota']=evaluacion_obj.presupuesto
            evaluacion_objDir['observacion_presupuesto_str']=evaluacion_obj.observacion_presupuesto

            evaluacion_objDir['replicabilidad_nota']=evaluacion_obj.replicabilidad
            evaluacion_objDir['observacion_replicabilidad_str']=evaluacion_obj.observacion_problematica

            evaluacion_objDir['evaluador']=u"%s %s" %(evaluacion_obj.usuario.first_name,evaluacion_obj.usuario.last_name)

            evaluacion_objDir['fecha'] = evaluacion_obj.D_fechaCreacion.strftime('%Y-%m-%d')
            json.append(evaluacion_objDir)
        except:
            pass

        return JsonResponse(json,safe=False)
    return JsonResponse(json,safe=False)



