from django.conf.urls import url
from Proyectos import views


urlpatterns = [
	url(r'^informacion/(?P<pk>[0-9]+)/$', views.info_post, name="informacion"),
	url(r'^postu_a_convo/(?P<pk>[0-9]+)/$', views.postulacion_a_convocatoria),
	url(r'^save_propu/', views.save_propuesta),
	url(r'^pdf/(?P<pdf_pk>[0-9]+)/$', views.generar_pdf,  name='generar_pdf'),
	url(r'^enviar_documentos/',views.enviar_documentos, name="enviar-documentos"),
	url(r'^get_documento_resultado/',views.get_documeto, name="get-get-documento-resultado"),
	url(r'^encuesta/',views.encuesta, name ="encuesta" ),
	url(r'^realizar_encuesta',views.realizar_encuesta, name ="realizar_encuesta"),
	url(r'^sugerencia', views.sugerencia),
	url(r'enviar_sugerencia', views.enviar_sugerencia, name= 'enviar_sugerencia'),
	url(r'^get_evaluacion/',views.get_evaluacion, name="get-evaluacion"),
	url(r'^guardar_actividad/', views.guardar_actividad),
	url(r'^get_datos_actividad/', views.get_datos_actividad),
	
]