# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.forms import ModelForm, Select
from Usuarios.models import ob_usuarios
from suit.widgets import AutosizedTextarea , EnclosedInput
from django.contrib.auth.admin import UserAdmin
from django.forms import TextInput, Textarea
from django.template.loader import render_to_string, get_template
from multiprocessing.dummy import Pool
from django.core.mail import EmailMultiAlternatives
from .models import *
from .views import *
import datetime
import time
# Register your models here.

# def enviar_correo_evaluador(propuesta, email):
# 	fecha_actual = datetime.datetime.now()
# 	template_html = 'correo_evaluador.html'
# 	html_content = render_to_string(template_html, {'fecha': fecha_actual, 'proyecto': propuesta})
# 	subject = u" adiuvocolombia - ¡TIENES UN NUEVO PROYECTO ASIGNADO EN ADIUVO!"
# 	text_content = '' 
# 	from_email = '"No responder " <noreply.adiuvo@gmail.com>'
# 	to = email
# 	msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
# 	msg.attach_alternative(html_content, "text/html")
# 	msg.send()


# def enviar_correo_transaccion(email):
# 	fecha_actual = datetime.datetime.now()
# 	template_html = 'correo_transaccion.html'
# 	html_content = render_to_string(template_html, {'fecha': fecha_actual})
# 	subject = u" adiuvocolombia - ¡LOS RECURSOS ESTÁN EN CAMINO!"
# 	text_content = '' 
# 	from_email = '"No responder " <noreply.adiuvo@gmail.com>'
# 	to = email
# 	msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
# 	msg.attach_alternative(html_content, "text/html")
# 	msg.send()


# def callback_correo(result):
# 	print ("Mensaje enviado")
# 	return True

class vista_convocatoria(admin.ModelAdmin):

	list_display = ('ver_foto','con_nombre','con_descripcion','descargar_terminos','descargar_problematica')
	list_filter = ('date','con_estado',)
	list_select_related = True
	search_fields = ('con_problematica','usuario__first_name','usuario__last_name','usuario__username')


	def save_model(self, request, obj, form, change):
		if change==False:
			obj.usuario = request.user
		obj.save()


	fieldsets = [
						(None, {
							'classes': ('suit-tab', 'suit-tab-general'),
							'fields': [
									'con_terminos',
									'con_nombre',
									'con_descripcion',
									'con_imagen',
									'con_problematica',
									'con_estado',
							]
						}),		
					]



	def get_readonly_fields(self, request, obj=None):
	# make all fields readonly
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "ALIADOS":
				readonly_fields = ['con_terminos','con_nombre','con_descripcion','con_imagen','con_problematica','con_estado','date']
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields
			elif (request.user.is_superuser) :
				return []
			elif request.user.groups.all()[0].name == "POSTULANTE":
				readonly_fields = ['con_terminos','con_nombre','con_descripcion','con_imagen','con_problematica','con_estado','date']
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields
			else:
				return []
		elif (request.user.is_superuser) :
			return []


	class Media:
		js = ('js/jquery-3.1.1.min.js', 'js/admin/centrar.js',)


	
	def changelist_view(self, request):
		if request.user.is_superuser:
			self.list_display = ('ver_foto','con_nombre','ver_descripcion', 'con_estado')
			return super(vista_convocatoria, self).changelist_view(request)
		else:
			self.list_display = ('date', 'ver_foto','con_nombre','ver_descripcion', 'descargar_terminos', 'descargar_problematica', 'date2', 'con_estado', 'btn_postulacion')
		return super(vista_convocatoria, self).changelist_view(request)


	# def save_model(self, request, obj, form, change): 
	#   if change == False: 
	#       obj.user = request.user
	#       obj.save()


	
	# fieldsets = (
	#   (None, {
	#       'fields': ('con_terminos','con_nombre','con_imagen','con_descripcion','con_problematica','con_estado','date','date2')
	#   }),
	# )

	# def has_add_permission(self,request):
	#   if request.user.is_superuser == False:          
	#       return False 
	#   else: 
	#       return True

	def ver_descripcion(self, obj):
		return format_html ('<div style = "white-space : pre"><p style =" white-space: wrap; width: 300px;">'+obj.con_descripcion + ' </p></div>')
	ver_descripcion.short_description = "Descripción"

	def descargar_terminos(self, obj):
		return format_html('<a target="_blank" style="display: inline-block;background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;"  id= "paso_1_'+
			str(obj.pk)+'" href="'+obj.con_terminos.url+'">Descargar</a> <hr> <input type="checkbox"   onchange="cargar_paso('+str(obj.pk)+')" id="paso_1_c_'+str(obj.pk)+
			'" value="True"> <label for="cbox2">Leí y acepto los Términos de Referencia </label>')
	descargar_terminos.short_description="Paso 1: Leer Términos de Referencia"

	def descargar_problematica(self, obj):
			return format_html('<a target="_blank" style=" display: inline-block;background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;" id= "paso_2_'
				+str(obj.pk)+'" href="'+obj.con_problematica.url+'">Descargar</a><hr> <input type="checkbox"   onchange="cargar_paso('+str(obj.pk)+')" id="paso_2_c_'+str(obj.pk)
				+'" value="True"> <label for="cbox2">Hice el diagnóstico de la problemática con la comunidad</label>')
	descargar_problematica.short_description="Paso 2: Identificar la problemática"

	def btn_postulacion(self, obj):
		return format_html('<a target="_blank" style="display: inline-block; background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;"  id= "paso_1_'
		+str(obj.pk)+'"  href="/admin/postu_a_convo/'+str(obj.pk)+'/">Postula</a>')
	btn_postulacion.short_description="Paso 3: Postular"
	


"""  _________________________________________________PROPUESTAS ________________________  """
class equipos_model(ModelForm):
	class Meta:
		widgets = {
			'eq_nombre' : AutosizedTextarea ,
			'eq_funciones' : AutosizedTextarea ,
			'eq_telefono' : AutosizedTextarea ,
			'eq_correo' : AutosizedTextarea ,
			'eq_resena' : AutosizedTextarea ,
			
		}

class actividades_model(ModelForm):
	class Meta:
		widgets = {
			'act_actividades' : AutosizedTextarea ,
			'act_metodologia' : AutosizedTextarea ,
			'act_tiempo' : AutosizedTextarea ,
			'act_riesgo' : AutosizedTextarea ,
			
		}

class equipos_inline(admin.TabularInline):
	form = equipos_model
	model = miembros_equipo
	extra = 1
	suit_classes = 'suit-tab suit-tab-general'
	verbose_name_plural = 'Miembros del Equipo'
	sortable = 'position'
	fields = ('eq_nombre','eq_funciones','eq_telefono','eq_correo','eq_resena')
	def get_readonly_fields(self, request, obj=None):
	# make all fields readonly
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				if (obj):
					if (obj.prop_estado=="NS" or obj.prop_estado=="NE"):
						readonly_fields = ['eq_nombre','eq_funciones','eq_telefono','eq_correo','eq_resena']
					else:
						readonly_fields = ['usuario']
				else:
					readonly_fields = []
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields

			elif (request.user.is_superuser) :
				return []
			else:
				return []
		elif (request.user.is_superuser) :
			return []


class actividades_inline(admin.TabularInline): 
	form = actividades_model
	model = actividade
	extra = 1
	suit_classes = 'suit-tab suit-tab-actividades'
	verbose_name_plural = 'Actividades'
	sortable = 'position'
	fields = ('act_actividades','act_metodologia','act_tiempo','act_presupuesto','act_riesgo')
	def get_readonly_fields(self, request, obj=None):
	# make all fields readonly
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				if (obj):
					if (obj.prop_estado=="NS" or obj.prop_estado=="NE"):
						readonly_fields = ['act_actividades','act_metodologia','act_tiempo','act_presupuesto','act_riesgo']
					else:
						readonly_fields = ['usuario']
				else:
					readonly_fields = []
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields

			elif (request.user.is_superuser) :
				return []
			else:
				return []
		elif (request.user.is_superuser) :
			return []



class material_audiovisual_inline(admin.TabularInline):
	model = material_audiovisual
	extra = 1
	suit_classes = 'suit-tab suit-tab-general'
	verbose_name_plural = 'Materiales audiovisuales'
	sortable = 'position'
	fields = ('mat_imagen','mat_descripcion')
	def get_readonly_fields(self, request, obj=None):
	# make all fields readonly
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				if (obj):
					if (obj.prop_estado=="NS" or obj.prop_estado=="NE"):
						readonly_fields = ['mat_imagen','mat_descripcion']
					else:
						readonly_fields = ['usuario']
				else:
					readonly_fields = []
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields

			elif (request.user.is_superuser) :
				return []
			else:
				return []
		elif (request.user.is_superuser) :
			return []

class impactos_inline(admin.TabularInline):
	model = impactos
	extra = 1
	suit_classes = 'suit-tab suit-tab-res'
	verbose_name_plural = 'Impactos esperados'
	sortable = 'position'
	fields = ('imp_determinantes','imp_ustedes','imp_cuantos','imp_impactos')
	def get_readonly_fields(self, request, obj=None):
	# make all fields readonly
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				if (obj):
					if (obj.prop_estado=="NS" or obj.prop_estado=="NE"):
						readonly_fields = ['imp_determinantes','imp_ustedes','imp_cuantos','imp_impactos']
					else:
						readonly_fields = ['usuario']
				else:
					readonly_fields = []
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields

			elif (request.user.is_superuser) :
				return []
			else:
				return []
		elif (request.user.is_superuser) :
			return []
# class institucion_inline(admin.TabularInline):
#   model = propuesta
#   filter_horizontal = ('ins_institucion',) 



class proyecto_model(ModelForm):
	class Meta:
		widgets = {
			'prop_resumen_desafio': AutosizedTextarea,
			'prop_justificacion': AutosizedTextarea,
			
		}

class vista_propuesta(admin.ModelAdmin): 
	form = proyecto_model
	list_display = ('prop_titulo','localizacion','fecha','prop_estado','puntaje')
	search_fields = ('prop_resumen_desafio','usuario__first_name','usuario__last_name','usuario__username')
	filter_horizontal = ('ins_institucion',)

	list_select_related = True

	suit_form_includes = (
		('proyecto/info_proyecto.html', 'top', 'general'),
		('proyecto/info_actividades.html','top','actividades'),
		('proyecto/info_resultados.html','top','r_esperado'),
		('proyecto/info_general.html','middle','general'),
		)

	def formfield_for_choice_field(self, db_field, request, **kwargs):
			if db_field.name == "prop_estado":
				try:
					if request.user.is_superuser:
						kwargs['choices'] = (
							('PD',"Por discutir"),
							('NS',"No seleccionado"),
							('EL',"Elegible"),
							)
				except Exception as e:
					pass
					#kwargs['choices'] += (('ready', 'Ready for deployment'),)
			return super(vista_propuesta, self).formfield_for_choice_field(db_field, request, **kwargs)

	def fecha(self, obj):
		return obj.prop_fechaCreacion.strftime("%Y-%m-%d")
	fecha.short_description=" Fecha creación"

	def btn_evaluar(self, obj):
		try:
			evaluacion_obj = evaluacion.objects.get(propuesta=obj)
			return format_html('<a style=" display: inline-block;background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;"  href="/admin/Proyectos/evaluacion/'+(u"%s" %(str(evaluacion_obj.pk)))+'/change">Evaluar</a>')
		except Exception as e:
			return format_html('<a style=" display: inline-block;background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;"  href="/admin/Proyectos/evaluacion/add/?pk_p='+str(obj.pk)+'&n_pro='+(u'%s' %(obj.prop_titulo))+'&correo='+(u'%s' %(obj.user.email))+'">Evaluar</a>')
	btn_evaluar.short_description="Evaluar"

	def btn_informe(self, obj):
		return format_html('<a target="_blank" style="display: inline-block; background-color: #007ee5; padding: 8px;border-radius: 4px; color: white;"  id= "paso_1_'+
			str(obj.pk)+'"href="/admin/informacion/'+str(obj.pk)+'/">Informe de avance</a>')
	btn_informe.short_description="Realizar informe de avance"

	def get_queryset(self, request):
		qs = super(vista_propuesta, self).get_queryset(request)
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				self.list_editable = ()
				self.list_display = ('prop_titulo','fecha','localizacion','puntaje','prop_estado','get_presupuesto','btn_informe')
				self.list_filter = ('prop_fechaCreacion','prop_estado','convo_nombre','tem_tematicas')
				self.suit_list_filter_horizontal = ('prop_estado','convo_nombre','tem_tematicas')
				try:
					return qs.filter(user = request.user)
				except:
					used= qs.filter(pk =0)
			else:
				used=qs.filter(pk =0)

		elif request.user.is_superuser: 
			self.list_display = ('prop_titulo','fecha','localizacion','puntaje','prop_estado','get_presupuesto','btn_informe','btn_evaluar')
			self.list_editable = ('prop_estado',)
			self.list_filter = ('prop_fechaCreacion','prop_estado','departamento_fk','convo_nombre','tem_tematicas')
			self.suit_list_filter_horizontal = ('prop_estado','departamento_fk','convo_nombre','tem_tematicas')
			return qs
	# def has_add_permission(self,request):
	#   if request.user.is_superuser == False:          
	#       return False 
	#   else: 
	#       return True


	def get_readonly_fields(self, request, obj=None):
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":

				if (obj==None):
					readonly_fields = ['puntaje','presupuesto','usuario','fecha_transferencia','monto','recibio','prop_estado']
				else:
					if (obj.prop_estado=="NS" or obj.prop_estado=="NE"):
						readonly_fields = ['convo_nombre','prop_titulo','descripcion','usuario','presupuesto','puntaje','D_fechaCreacion','justificacion','accion','metodologia','es_departamento_fk','evaluador_fk','documentacion','seguimiento','Participantes_red','instituciones_educativas','fecha_transferencia','monto','recibio','es_municipio_fk','es_corregimiento','prop_estado'] 
					else:
						readonly_fields = ['puntaje','presupuesto','usuario','fecha_transferencia','monto','recibio','prop_estado']

				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields
			else:
				return []
		elif (request.user.is_superuser) :
			return []	

	def save_model(self, request, obj, form, change): 
		if change == False: 
			obj.user = request.user
			# proyecto_obj = propuesta.objects.get(id=obj.id)
			# if proyecto_obj.monto != obj.monto and obj.monto > 0:
			# 	pool = Pool(processes=1) 
			# 	pool.apply_async(
			# 		enviar_correo_transaccion,
			# 		args=[ obj.user.email],
			# 		callback=callback_correo
			# )
			obj.save()
		else : 
			obj.save()


	def get_presupuesto (self, obj):
		total=0
		retornar= ""
		color =""
		try:
			actividades_preparacion = actividade.objects.filter(act_propuesta=obj)
			for act_pre in actividades_preparacion:
				total = total + float(act_pre.act_presupuesto)
			if(obj.presupuesto!=None and obj.presupuesto!=""):
				total =  float(obj.presupuesto) - total
				if(total>=0):

					retornar = "Presupuesto restante: $"+str('{:,}'.format(total))+ "<br>Presupuesto total: $"+str('{:,}'.format(float(obj.presupuesto)))
					color ="#007ee5"
				else:
					retornar = "Presupuesto restante: $"+str('{:,}'.format(total))+ "<br>Presupuesto total: $"+str('{:,}'.format(float(obj.presupuesto)))
					color ="#d83535"
			else:
				retornar = "No tiene presupuesto asignado"
				color ="#007ee5"
		except Exception as e:
			retornar = "Problema con el presupuesto, consulte al administrador"
			color ="#007ee5"
		return format_html('<p  style="padding: 3px;border-radius: 4px;color: white;background-color: '+color+';text-align: center; width: 100%; margin: auto; 0">'+retornar+'</p>')
			
	get_presupuesto.short_description="Presupuesto"

	class Media:
		js = ('js/admin/adminPresupuesto.js')

	def localizacion(self, obj):
		pro_departamento =""
		pro_municipio =""
		pro_vereda =""
		final_str =""

		try:
			pro_departamento  = obj.departamento_fk.A_nombre
		except Exception as e:
			pass

		try:
			pro_municipio = obj.municipio_fk.A_nombre
		except Exception as e:
			pass

		try:
			pro_vereda = obj.vereda
		except Exception as e:
			pass
		if (pro_departamento!="" and pro_departamento!=None):
			final_str += pro_departamento
			if (pro_municipio != "" and pro_municipio != None):
				final_str += " / "+pro_municipio
				if(pro_vereda!= "" and pro_vereda!=None and pro_municipio!=pro_vereda):
					final_str += " / "+pro_vereda
		else:
			final_str  = "Sin registrar"
		return format_html(final_str)
	localizacion.short_description=" Localización"

	def get_form(self, request, obj=None, **kwargs):
		montar_re_ejecutado =[] 
		if request.user.is_superuser:			
			if(obj): 

				self.suit_form_tabs = (('general', "Datos del proyecto"),('actividades', "Actividades y Presupuesto"), ('r_esperado', "Resultados"),('p_riesgos', "Posibles Riesgos"),)
			else:
				self.suit_form_tabs = (('general', "Datos del proyecto"),('actividades', "Actividades y Presupuesto"), ('r_esperado', "Resultados"),('p_riesgos', "Posibles Riesgos"),)

			self.inlines= [actividades_inline,equipos_inline,material_audiovisual_inline,impactos_inline]



			self.fieldsets = [
								(None, {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
											'convo_nombre',
											'prop_titulo',
											'prop_resumen_desafio',
											'prop_pregunta_desafio',
											'tem_tematicas',
											'prop_justificacion',
											'prop_solucion',
											'prop_estado',
											'presupuesto',
											'puntaje',
											'user',	
											'prop_metodologia',
											'prop_determinantes',



									]
								}),
								("Localización", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'departamento_fk',
										'municipio_fk',
										'es_vereda',
									]
								}),
								("Participantes", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'aportante',
										'pro_aliados',
									]
								}),
								("Instituciones educativas involucradas", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'ins_institucion',
									]
								}),

								("Transferencia", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'fecha_transferencia',
										'monto',
										'recibio',
									]
								}),
							]

		else:
			if(obj): 
				if(obj.prop_estado == "E"):
					montar_re_ejecutado = []
				if request.user.groups.all()[0].name == "POSTULANTE":
					if (obj.prop_estado==None or obj.prop_estado=="SP" or obj.prop_estado=="NEV"):
						self.inlines= [actividades_inline,equipos_inline,material_audiovisual_inline,impactos_inline]
					else:
						self.inlines= [actividades_inline,equipos_inline,material_audiovisual_inline,impactos_inline]
					if (obj.prop_estado=="EE"):
						self.suit_form_tabs = (('actividades', "Actividades y Presupuesto"),)
					elif (obj.prop_estado=="E"):
						self.suit_form_tabs = (('r_esperado', "Resultados"),)
					else:
						self.suit_form_tabs = (('general', "Datos del proyecto"),('actividades', "Actividades y Presupuesto"), ('r_esperado', "Resultados"),('p_riesgos', "Posibles Riesgos"),)
				else:
					self.suit_form_tabs = (('general', "Datos del proyecto"),('actividades', "Actividades y Presupuesto"), ('r_esperado', "Resultados"),('p_riesgos', "Posibles Riesgos"),)
			else:
				self.inlines= [actividades_inline,equipos_inline,material_audiovisual_inline,impactos_inline]
				self.suit_form_tabs = (('general', "Datos del proyecto"),('actividades', "Actividades y Presupuesto"), ('r_esperado', "Resultados"),('p_riesgos', "Posibles Riesgos"),)

				if request.user.groups.all()[0].name == "POSTULANTE":
					self.fieldsets = [
								(None, {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
											'convo_nombre',
											'prop_titulo',
											'prop_resumen_desafio',
											'prop_pregunta_desafio',
											'tem_tematicas',
											'prop_justificacion',
											'prop_solucion',
											'prop_estado',
											'presupuesto',
											'puntaje',
											'user',	
											'prop_metodologia',
											'prop_determinantes',

									]
								}),
								("Localización", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'departamento_fk',
										'municipio_fk',
										'es_vereda',
									]
								}),
								("Instituciones educativas involucradas", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'ins_institucion',
									]
								}),

								("Transferencia", {
									'classes': ('suit-tab', 'suit-tab-general'),
									'fields': [
										'fecha_transferencia',
										'monto',
										'recibio',
									]
								}),
		
								("Resultados: debes revisar este documento para poder hacer una evaluación correcta de los resultados. Es una guía útil para que empieces a medir lo que has alcanzado con tu proyecto.", {
									'classes': ('suit-tab', 'suit-tab-r_esperado'),
									'fields':montar_re_ejecutado
									}),
							]
				else:
					pass
		return super(vista_propuesta,self).get_form(request, obj=None, **kwargs)




"""  _________________________________________________DOCUMENTOS  ________________________  """

class documento_model(ModelForm):
	class Meta:
		widgets = {
			'codigo': EnclosedInput(prepend='DOCUMENTO_'),
		}
class documento_model_link(ModelForm):
	class Meta:
		widgets = {
			'codigo': EnclosedInput(prepend='fa-link'),
		}


class documentos_importante_admin(admin.ModelAdmin):
	
	list_display = ('descripcion','documento','D_fechaCreacion','estado','codigo')
	list_filter = ('D_fechaCreacion','estado',)
	search_fields = ('descripcion',)
	suit_list_filter_horizontal=('codigo')
	form = documento_model

	def get_readonly_fields(self, request, obj=None):
		if (obj):
			readonly_fields = ['codigo',] 
		else:
			readonly_fields = [] 
		if 'is_submitted' in readonly_fields:
			readonly_fields.remove('is_submitted')
		return readonly_fields


	def save_model(self, request, obj, form, change):
		fecha = datetime.datetime.now()
		marca_de_tiempo = time.mktime(fecha.timetuple())
		if change==False:
			obj.codigo = str("DOCUMENTO_"+obj.codigo)
		obj.save()


"""  _________________________________________________  EVALUACION ________________________  """

class evaluacion_model(ModelForm):
	class Meta:
		widgets = {
			'observacion_problematica': AutosizedTextarea,
			'observacion_justificacion': AutosizedTextarea,
			'observacion_accion': AutosizedTextarea,
			'observacion_metodologia': AutosizedTextarea,
			'observacion_r_esperado': AutosizedTextarea,
			'observacion_actividades': AutosizedTextarea,
			'observacion_posibles_riesgos': AutosizedTextarea,
			'observacion_beneficiarios': AutosizedTextarea,
			'observacion_presupuesto': AutosizedTextarea,
			'observacion_replicabilidad': AutosizedTextarea,
		}

class evaluacion_admin(admin.ModelAdmin):
	form = evaluacion_model

	suit_form_includes = (
		('proyecto/info_evaluacion.html', 'top', 'general'),
		('proyecto/info_skype.html', 'middle', 'general'),
		)

	
	list_display = ('problematica','justificacion','accion','metodologia','r_esperado','actividades','posibles_riesgos',)
	list_filter = ('D_fechaCreacion',)

	fieldsets = [
				(None, {
					'classes': ('suit-tab', 'suit-tab-general'),
						'fields': [
								('proyecto'),
								('problematica','observacion_problematica'),
								('justificacion','observacion_justificacion'),
								('accion','observacion_accion'),
								('beneficiarios','observacion_beneficiarios'),
								('metodologia','observacion_metodologia'),
								('r_esperado','observacion_r_esperado'),
								('actividades','observacion_actividades'),
								('presupuesto','observacion_presupuesto'),
								('replicabilidad','observacion_replicabilidad'),
								('posibles_riesgos','observacion_posibles_riesgos')
						]
				}),
			]

	class Media:
		js = ('js/admin/evaluacion.js',)

	def save_model(self, request, obj, form, change):
		if change==False:
			obj.usuario = request.user
			try:
				proyecto_obj = propuesta.objects.get(pk=obj.proyecto.pk)
				puntaje = obj.problematica + obj.justificacion + obj.accion + obj.beneficiarios + obj.metodologia + obj.r_esperado + obj.actividades + obj.posibles_riesgos + obj.replicabilidad + obj.presupuesto
				proyecto_obj.puntaje = puntaje
				if (puntaje>=81):
					proyecto_obj.prop_estado = "EL"
				if(puntaje<=59):
					proyecto_obj.prop_estado = "NS"
				elif (puntaje>59 and puntaje<81):
					proyecto_obj.prop_estado = "PD"
				
				proyecto_obj.save()
				obj.save()
			except Exception as e:
				pass  


	def get_readonly_fields(self, request, obj=None):
		if len(request.user.groups.all()) > 0:
			if request.user.groups.all()[0].name == "POSTULANTE":
				readonly_fields = ['usuario','proyecto','problematica','observacion_problematica','justificacion','observacion_justificacion','accion','observacion_accion','metodologia','observacion_metodologia','r_esperado','observacion_r_esperado','actividades','observacion_actividades','posibles_riesgos','observacion_posibles_riesgos','beneficiarios','observacion_beneficiarios','presupuesto','observacion_presupuesto','replicabilidad','observacion_replicabilidad'] 
				if 'is_submitted' in readonly_fields:
					readonly_fields.remove('is_submitted')
				return readonly_fields
			else:
				return []
		elif (request.user.is_superuser) :
			return []

	def response_add(self, request, new_object):
		return	redirect('/admin/Proyectos/propuesta/')

	def response_change(self, request, new_object):
		return   redirect('/admin/Proyectos/propuesta/')



"""  _________________________________________________  SUGERENCIAS ________________________  """
class sugerencias_admin(admin.ModelAdmin):
	"""docstring for ClassName"""
	list_display = ('user', 'Sugerencia')
	list_filter = ('user',)


	def get_readonly_fields(self, request, obj=None):
		if request.user.is_superuser:
			readonly_fields = ['user','Sugerencia']
			return readonly_fields
		else: 
			readonly_fields = ['user']
			return readonly_fields

	def get_queryset(self, request): 
		# For Django < 1.6, override queryset instead of get_queryset
		qs = super(sugerencias_admin, self).get_queryset(request)
		if request.user.is_superuser:
			return qs
		else:
			return qs.filter(user = request.user)

		
	def save_model(self, request, obj, form, change): 
		if change == False: 
			obj.user = request.user
			obj.save()
		else : 
			obj.save()

"""  _________________________________________________  ENCUESTAS ________________________  """
class encuestas_admin(admin.ModelAdmin):
	"""docstring for encuestas_admin"""
	list_display = ('usuario','pregunta1')
	list_filter = ('pregunta1',)

	fieldsets = (
		('Con respecto a la plataforma', {
			'fields': ('pregunta1', 'Pregunta2', 'Pregunta3', 'Pregunta4')
		}),
		('Con respecto al proceso', {
			'fields': ('Pregunta5', 'Pregunta6','Pregunta7','Pregunta8'),
		}),
	)

	def get_queryset(self, request): 
		# For Django < 1.6, override queryset instead of get_queryset
		qs = super(encuestas_admin, self).get_queryset(request)
		if request.user.is_superuser:
			return qs
		else:
			return qs.filter(usuario = request.user)

	def save_model(self, request, obj, form, change): 
		if change == False: 
			obj.user = request.user
			obj.save()
		else : 
			obj.save()

	def get_readonly_fields(self, request, obj=None):
		if request.user.is_superuser:
			readonly_fields = ['usuario','pregunta1','Pregunta2','Pregunta3','Pregunta4','Pregunta5','Pregunta6','Pregunta7','Pregunta8']
			return readonly_fields
		else: 
			readonly_fields = ['usuario']
			return readonly_fields


		
	

admin.site.register(convocatoria, vista_convocatoria)
admin.site.register(propuesta,vista_propuesta)
admin.site.register(institucion)
admin.site.register(propuesta_tematica)
admin.site.register(evaluacion,evaluacion_admin)
admin.site.register(documentos_importante,documentos_importante_admin)
admin.site.register(encuestas,encuestas_admin)
admin.site.register(sugerencias,sugerencias_admin)
