# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.html import format_html
from django.db import models
from datetime import datetime
from django.utils import timezone
from multiselectfield import MultiSelectField
from django.contrib.auth.models import Group

from redactor.fields import RedactorField
from smart_selects.db_fields import ChainedForeignKey
from Usuarios.models import *



class IntegerRangeField(models.IntegerField):
	def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
		self.min_value, self.max_value = min_value, max_value
		models.IntegerField.__init__(self, verbose_name, name, **kwargs)
	def formfield(self, **kwargs):
		defaults = {'min_value': self.min_value, 'max_value':self.max_value}
		defaults.update(kwargs)
		return super(IntegerRangeField, self).formfield(**defaults)

class convocatoria(models.Model):
	con_choice = (
	('A', "Abierta"),
	('C', "Cerrada"),
	)
	usuario          = models.ForeignKey("Usuarios.ob_usuarios",blank=True,null=True, related_name="usuario+")
	con_terminos     = models.FileField(upload_to='uploads/',  verbose_name='terminos de referencia')
	con_nombre       = models.CharField(verbose_name='nombre convocatoria', max_length=255 ,blank=True, null= True)
	con_descripcion  = models.TextField(verbose_name = 'Descripcion',blank=True, null= True)
	con_problematica = models.FileField(verbose_name='Metodologia: problematica')
	con_estado       = models.CharField(verbose_name="Estado",choices = con_choice,max_length=255,null=True,blank=True) 
	con_imagen       = models.ImageField(verbose_name='Logo convocatoria', upload_to='fotos/', blank=True, null= True)
	date             = models.DateTimeField('Fecha inicio convocatoria',default=datetime.now)
	date2            = models.DateTimeField('Fecha fin convocatoria',default=datetime.now)
	aportante        = models.ForeignKey('Usuarios.ob_usuarios', limit_choices_to={'groups__name': "APORTANTE"}, verbose_name='Aportante', blank=True, null=True)
	con_aliados      = models.ForeignKey('Usuarios.ob_usuarios', limit_choices_to={'groups__name': "ALIADOS"}, related_name="aliado+", verbose_name='Aliados',blank=True, null=True)

	def ver_foto(self):                                            
		if (self.con_imagen and hasattr(self.con_imagen, 'url')):
			return format_html('<img src="%s" height="100px" width="150px"/>'%(self.con_imagen.url))
	ver_foto.short_description = ' Logo de Convocatoria'


	def __str__(self):
		return self.con_nombre
	class Meta:
		"""docstring for Meta"""
		verbose_name = u'Convocatoria'
		verbose_name_plural = u'Convocatorias'

class institucion(models.Model):
	"""docstring for institucion"""
	ins_nombre = models.CharField(verbose_name='nombre de institución ', max_length=255 ,blank=True, null= True)
	ins_direccion = models.CharField(verbose_name='Dirección de institución ', max_length=255 ,blank=True, null= True)
	
	def __str__(self):
		return self.ins_nombre
	class Meta:
		"""docstring for Meta"""
		verbose_name = u'institución'
		verbose_name_plural = u'Instituciones'

class propuesta(models.Model):
	day  = timezone.now()
	formatedDay  = day.strftime("%d/%m/%Y")

	user = models.ForeignKey("Usuarios.ob_usuarios",blank=True,null=True)
	Dete = ((1, 'Curriculum'),
			(2, 'Espacios'),
			(3, 'Herramientas y Procesos'),
			(4, 'Sistemas'))
	status=(
		('SP',"No postulado"),
		('NEV',"No evaluado"),
		('NS',"No seleccionado"),
		('EL',"Elegible"),
		('A',"En acción"),
		('EE',"En ejecución"),
		('E',"Ejecutado"),
		('NE',"No ejecutado"),
		('PD','Por discutir')
	)
	presupuesto = models.DecimalField(max_digits = 18, decimal_places= 3, verbose_name = "Presupuesto", null=True, blank=True)
	puntaje = models.DecimalField(max_digits = 5, decimal_places = 2,verbose_name="Puntaje",null=True,blank=True)
	prop_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación",auto_now_add=True, blank = True, null=True)
	prop_titulo = models.CharField(verbose_name='Titulo Propuesta', max_length=255 )
	prop_resumen_desafio = RedactorField(verbose_name = 'Resumen', )
	prop_pregunta_desafio = models.CharField(verbose_name='Pregunta desafío', max_length=255)
	prop_determinantes = MultiSelectField(verbose_name='Determinantes', choices=Dete,max_choices=4,max_length=50)
	prop_justificacion = RedactorField(verbose_name='Justificacion', blank=True, null= True)
	prop_solucion = RedactorField(verbose_name='Solución' ,blank=True, null= True)
	prop_estado = models.CharField(verbose_name= 'Estado Proyecto', choices=status, max_length=50, default= 'SP')
	prop_metodologia = RedactorField(verbose_name=u'Metodología', blank=True,null=True)
	ins_institucion = models.ManyToManyField(institucion,verbose_name='Instituciones Educativas',blank=True)
	tem_tematicas = models.ForeignKey('Proyectos.propuesta_tematica',verbose_name='Tematica', default=1,blank=True, null=True)
	convo_nombre = models.ForeignKey(convocatoria, verbose_name= 'Nombre convocatoria',default= 1,blank=True, null=True)
	fecha_transferencia = models.DateField(blank=True, null=True, verbose_name="Fecha Transferencia")
	monto = models.DecimalField(max_digits=10, decimal_places=2,verbose_name="Monto",null=True,blank=True)
	recibio = models.CharField(verbose_name="Quien Recibió:",max_length=1000,null=True,blank=True)

	departamento_fk = models.ForeignKey('localizacion.departamento', verbose_name="Departamento",blank=True,null=True)
	municipio_fk = ChainedForeignKey(
		'localizacion.municipio', 
		chained_field="departamento_fk",
		chained_model_field="es_departamento_pk", 
		show_all=False, 
		auto_choose=True,
		sort=True,
		verbose_name ="Municipio",null = True, blank =True
	)
	es_vereda = models.CharField(verbose_name="Vereda",max_length=500,null = True, blank =True)
	
	aportante = models.ForeignKey('Usuarios.ob_usuarios', limit_choices_to={'groups__name': "APORTANTE"},  related_name="aportanteproyect+",verbose_name='Aportante', blank=True, null=True)
	pro_aliados  = models.ForeignKey('Usuarios.ob_usuarios', limit_choices_to={'groups__name': "ALIADOS"}, related_name="aliadosProyect+", verbose_name='Aliados',blank=True, null=True)

	def __str__(self):
		return self.prop_titulo

	def __unicode__(self): 
		return self.prop_titulo
		
	class  Meta:
		verbose_name =u'Propuesta'
		verbose_name_plural=u'Propuestas'

class propuesta_tematica(models.Model): 
	nombre = models.CharField(verbose_name='Tematica:', max_length=255, blank=True, null=True)
	f_creacion = models.DateTimeField(verbose_name= 'fecha creacion', auto_now_add=True, blank=True, null= True)

	def __unicode__(self):
			return u"%s" %(self.nombre)

	def __str__(self):
			return u"%s" %(self.nombre)
	class Meta:
		verbose_name=u'Tematica'
		verbose_name_plural=u'Tematicas'

class actividade(models.Model):
	act_propuesta = models.ForeignKey (propuesta,blank=True,null=True)
	act_actividades = models.CharField(verbose_name='Actividades', max_length=255 ,blank=True, null= True)
	act_metodologia = models.CharField(verbose_name='Metodología', max_length=255 ,blank=True, null= True)
	act_tiempo = models.CharField(verbose_name='Tiempo', max_length=255 ,blank=True, null= True)
	act_presupuesto = models.DecimalField(verbose_name="Presupuesto",max_digits=10, decimal_places=2,blank=True, null= True)
	act_riesgo = models.CharField(verbose_name='Riesgo', max_length=255 ,blank=True, null= True)
	act_voluntario = models.BooleanField(verbose_name="Voluntario",default=False,blank=True)
	act_tipo_voluntario = models.TextField(verbose_name='Tipo de Voluntario', max_length=255 ,blank=True, null= True)
	act_cotizacion = models.ImageField(verbose_name='Cotizacion', max_length=255 ,blank=True, null= True)
	position = models.PositiveIntegerField(blank=True,null=True)

	def ver_foto(self): 
		if (self.act_cotizacion and hasattr(self.act_cotizacion, 'url')):
			return format_html('<img src="%s" height ="80px" width = "70px" />' %(self.act_cotizacion.url))
	ver_foto.short_description = 'Cotizacion'

	def __str__(self):
		return self.act_actividades
	class Meta:
		"""docstring for Meta"""
		verbose_name = u'Actividad'
		verbose_name_plural = u'Actividades'	

class miembros_equipo(models.Model): 
	eq_propuesta = models.ForeignKey (propuesta,blank=True,null=True)
	eq_nombre  = models.CharField(verbose_name='Nombre', max_length=255 ,blank=True, null= True)
	eq_funciones  = models.CharField(verbose_name='Funciones del equipo', max_length=255 ,blank=True, null= True)
	eq_telefono = models.CharField(verbose_name='Telefono', max_length=255 ,blank=True, null= True)
	eq_correo = models.CharField(verbose_name='Correo', max_length=255 ,blank=True, null= True)
	eq_resena  = models.TextField(blank=True,null=True,verbose_name="Reseña")
	position = models.PositiveIntegerField(blank=True,null=True)
	def __str__(self):
		return self.eq_nombre
	class Meta:
		"""docstring for Meta"""
		verbose_name = u'Miembro del equipo'
		verbose_name_plural = u'Miembros del equipo'	

class impactos(models.Model):
	ustedes = ((1, 'Rector'),
				(2, 'Estudiantes'),
				(3, 'Profesores'),
				(4, 'Aulas'))
	Dete = ((1, 'Curriculum'),
			(2, 'Espacios'),
			(3, 'Herramientas y Procesos'),
			(4, 'Sistemas'))
	imp_propuesta = models.ForeignKey (propuesta, blank=True,null=True)
	imp_determinantes = MultiSelectField(verbose_name='Determinantes', choices=Dete,max_choices=3,max_length=3,blank=True, null= True)
	imp_ustedes = models.CharField(verbose_name='Ustedes', choices=ustedes, max_length=255 ,blank=True, null= True)
	imp_cuantos = models.IntegerField(verbose_name='Cuantos ',blank=True, null= True)
	imp_impactos = models.CharField(verbose_name='Impactos esperados', max_length=255 ,blank=True, null= True)
	position = models.PositiveIntegerField(blank=True,null=True)
	def __str__(self):
		return self.imp_impactos
	class Meta:
		"""docstring for Meta"""
		verbose_name = u'Impacto'
		verbose_name_plural = u'Impactos'		

class material_audiovisual(models.Model): 
	mat_propuesta = models.ForeignKey (propuesta,blank=True,null=True)
	mat_imagen = models.ImageField(verbose_name='Material Audiovisual', upload_to='fotos/', blank=True, null= True)
	mat_descripcion = models.CharField(max_length=255,blank=True,null=True,verbose_name="Descripción")
	position = models.PositiveIntegerField(blank=True,null=True)
	
	def ver_foto(self): 
		if (self.mat_imagen and hasattr(self.mat_imagen, 'url')):
			return format_html('<img src="%s" height ="80px" width = "70px" />' %(self.mat_imagen.url))
	ver_foto.short_description = 'Material audiovisual'

	class Meta:
		"""docstring for Meta"""
		verbose_name = u'Material Audiovisual'
		verbose_name_plural = u'Material Audiovisual'



class documentos_importante(models.Model):

	options=(
		('E',"Evaluadores"),
		('P',"POSTULANTE"),
		)
	visible_a   = models.CharField(verbose_name="Visible a",blank=True,null=True,choices=options,max_length=255)
	documento   = models.FileField(upload_to='uploads/',blank=True,null=True,verbose_name="Documento")
	descripcion = models.CharField(max_length=255,blank=True,null=True,verbose_name="Descripción")
	codigo      = models.CharField(max_length=255,blank=True,null=True,verbose_name="Código", unique=True)
	D_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación",auto_now_add=True, blank = True, null=True)
	estado      = models.BooleanField(verbose_name="Mostrar?", default=False)

	#---------------------CONVERTIR A MAYSCULA -------------------------
	def save(self, force_insert=False, force_update=False):
		self.codigo = self.codigo.upper()
		super(documentos_importante, self).save(force_insert, force_update)
	#------------------FIN CONVERTIR A MAYUSCULA ----------------------
	def __unicode__(self):
			return u"%s" %(self.documento)

	def __str__(self):
			return u"%s" %(self.documento)		
	class Meta:
		verbose_name=u'Documento'
		verbose_name_plural=u'Documentos importantes'


class encuestas(models.Model):
	usuario   = models.ForeignKey('Usuarios.ob_usuarios', blank=True , null = True)
	pregunta1 = models.PositiveIntegerField(verbose_name ='Del 1 - 5, ¿qué tan intuitiva es? (1 - poco intuitiva hasta 5 - muy intuitiva)', blank = True,  null = True)
	Pregunta2 = models.CharField (verbose_name='¿En qué se podría mejorar? Sé específico, por favor.', blank = True,  null = True, max_length=255 )
	Pregunta3 = models.CharField (verbose_name='¿Qué te gustaría que tuviera la plataforma de forma adicional?', blank = True,  null = True, max_length=255 )
	Pregunta4 = models.PositiveIntegerField(verbose_name ='Del 1 - 5, ¿te sentiste obligado/a o agradado/a al utilizar la plataforma? (1- obligado hasta 5 - agradado)', blank = True,  null = True)
	Pregunta5 = models.PositiveIntegerField(verbose_name ='Del 1 - 5, ¿qué tan claro fue el proceso? (1 - poco claro hasta 5 - muy claro)', blank = True,  null = True)
	Pregunta6 = models.PositiveIntegerField(verbose_name ='Del 1 - 5, ¿qué tan a gusto te sentiste con tu participación? (1 - poco a gusto  hasta 5 - muy a gusto)', blank = True,  null = True)
	Pregunta7 = models.CharField (verbose_name='Cuéntanos qué fue lo que te gustó, o no te gustó, del proceso.', blank = True,  null = True, max_length=255 )
	Pregunta8 = models.CharField (verbose_name='En una frase, ¿cómo definirías el impacto de tu participación?', blank = True,  null = True, max_length=255 )
	def __unicode__(self):
		return u"%s" %(self.pregunta1)
	def __str__(self):
		return  u"%s" %( self.pregunta1)

class sugerencias(models.Model):
	"""docstring for sugerencias"""
	user = models.ForeignKey('Usuarios.ob_usuarios', blank=True, null=True)
	Sugerencia = models.CharField(verbose_name = 'Sugerencia', blank=True, null=True, max_length=255)
	
	def __unicode__(self):
		return u"%s" %(self.user)
	def __str__(self):
		return  u"%s" %( self.user)


class evaluacion(models.Model):
	usuario=models.ForeignKey('Usuarios.ob_usuarios',verbose_name="Evaluador",blank=True,null = True, on_delete = models.SET_NULL)
	proyecto=models.ForeignKey(propuesta,verbose_name="Proyecto")
	
	problematica=IntegerRangeField(verbose_name="Problemática",min_value=0, max_value=15, help_text="Evaluar en el rango de 0 a 15")
	observacion_problematica=models.TextField(blank=True,null=True,verbose_name="Observación")
	
	justificacion=IntegerRangeField(verbose_name="Justificación",min_value=0, max_value=10, help_text="Evaluar en el rango de 0 a 10")
	observacion_justificacion=models.TextField(blank=True,null=True,verbose_name="Observación")
	
	accion=IntegerRangeField(verbose_name="Acción",min_value=0, max_value=20, help_text="Evaluar en el rango de 0 a 20")
	observacion_accion=models.TextField(blank=True,null=True,verbose_name="Observación")

	beneficiarios=IntegerRangeField(verbose_name="Beneficiarios",min_value=0, max_value=10, help_text="Evaluar en el rango de 0 a 10")
	observacion_beneficiarios=models.TextField(blank=True,null=True,verbose_name="Observación")

	metodologia=IntegerRangeField(verbose_name="Metodología",min_value=0, max_value=10, help_text="Evaluar en el rango de 0 a 10")
	observacion_metodologia=models.TextField(blank=True,null=True,verbose_name="Observación")


	r_esperado=IntegerRangeField(verbose_name="Resultados esperados",min_value=0, max_value=15, help_text="Evaluar en el rango de 0 a 15")
	observacion_r_esperado=models.TextField(blank=True,null=True,verbose_name="Observación")


	actividades=IntegerRangeField(verbose_name="Cronograma",min_value=0, max_value=5, help_text="Evaluar en el rango de 0 a 5")
	observacion_actividades=models.TextField(blank=True,null=True,verbose_name="Observación")

	presupuesto=IntegerRangeField(verbose_name="Presupuesto",min_value=0, max_value=5, help_text="Evaluar en el rango de 0 a 5")
	observacion_presupuesto=models.TextField(blank=True,null=True,verbose_name="Observación")


	replicabilidad=IntegerRangeField(verbose_name="Replicabilidad",min_value=0, max_value=5, help_text="Evaluar en el rango de 0 a 5")
	observacion_replicabilidad=models.TextField(blank=True,null=True,verbose_name="Observación")

	posibles_riesgos=IntegerRangeField(verbose_name="Posibles riesgos",min_value=0, max_value=5, help_text="Evaluar en el rango de 0 a 5")
	observacion_posibles_riesgos=models.TextField(blank=True,null=True,verbose_name="Observación")

	D_fechaCreacion = models.DateTimeField( verbose_name="Fecha creación",auto_now_add=True, blank = True, null=True)

	def __unicode__(self):
		return u"%s" %(self.proyecto)

	def __str__(self):
		return u"%s" %(self.proyecto)
	class Meta:
		verbose_name=u'Evaluación'
		verbose_name_plural=u'Evaluaciones'
		