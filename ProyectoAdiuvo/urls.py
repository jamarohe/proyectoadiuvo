
"""ProyectoAdiuvo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from Usuarios import views as UsuariosViews
from Proyectos import views
from Proyectos import urls
from localizacion import views
from localizacion import urls

urlpatterns = [
    url(r'^admin/',admin.site.urls),
    url(r'^admin/',include('Usuarios.urls')),
    url(r'^admin/',include('Proyectos.urls')),
    url(r'^redactor/', include('redactor.urls')),
    url(r'^propuesta/', include('Proyectos.urls')),
    url(r'^admin/', include('localizacion.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^enivar_notificaciones/',UsuariosViews.enivar_notificaciones, name="enivar-notificaciones"),
    url(r'^actualizar_noti/',UsuariosViews.actualizar_noti, name="actualizar_noti"),
    url(r'^guardar_notificaciones/',UsuariosViews.guardar_notificaciones, name="guardar-notificaciones"),
    #url(r'^reporte_proyectos_pdf/$',login_required(ReporteProyectosPDF.as_view()), name="reporte_proyectos_pdf"),
    # url(r'^advanced_filters/', include('advanced_filters.urls'))

]
if settings.DEBUG:
   urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
